function runVue(){

    // The object is added to a Vue instance
    const nameHandlers = Array.from(document.querySelectorAll(".app"))
    for (let handler of nameHandlers) {
        new Vue({
            el: handler,
            data: {
                data: {
                    endpoint: handler.getElementsByTagName('input')[0].value,
                    id: '',
                    parent: '',
                    name: ''
                },
            },
            methods: {
                sendDataToAPI: function (event) {
                    var data = this.data;
                    this.$http.post(data.endpoint, data).then(function (response) {
                        console.log(response.body);
                        document.getElementById('response').value = JSON.stringify(response.body)
                        this.loading = false;
                    }, function (response) {
                        console.log('Error!:', response.data);
                        this.loading = false;
                    });
                }
            }
        })
    }

    Vue.http.interceptors.push(function(request, next) {

        // modify headers
        request.headers.set('X-CSRF-TOKEN', '{!! csrf_token() !!}');

        // continue to next interceptor
        next();
    });
}