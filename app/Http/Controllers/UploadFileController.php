<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;

class UploadFileController extends Controller
{

    private $tree = [];

    public function index()
    {
        return view('uploadfile');
    }

    public function showUploadFile(Request $request)
    {
        # Get file
        $content = $this->getFileSplit($request);

        # Return as tree
        $tree_temp = $this->to_tree($content);

        # Make tree into array with proper padding. Store array in $this->tree
        $this->make_tree($tree_temp);

        return view('uploadfile', ['tree' => $this->tree]);
    }

    public function getFileSplit($request)
    {
        $file = $request->file('structured_tree');

        //Get content of the file
        $content = [];
        foreach ($file->openFile() as $line) {
            $cat =  explode("|", $line);
            $content[] = $cat;
        }
        asort($content);
        $content = array_values($content);

        return $content;
    }

    private function make_tree($tree, $padding='')
    {
        foreach ($tree as $leaf){
            if(is_array($leaf)){
                $this->make_tree($leaf, $padding . '-');
            }
            else{
                $this->tree[] = substr($padding.$leaf, 1);
            }
        }
    }

    private function to_tree($array)
    {
        $flat = array();
        $tree = array();

        foreach ($array as $useless => $arr) {
            $child = $arr[0];
            $parent = $arr[1];
            if (!isset($flat[$child])) {
                $flat[$child] = array($arr[2]);
            }
            if (!empty($parent)) {
                $flat[$parent][$child] =& $flat[$child];
            } else {
                $tree[$child] =& $flat[$child];
            }
        }
        return $tree;
    }
}