<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Relation;
use Illuminate\Database\QueryException;
use \Carbon\Carbon;

class EndpointController extends Controller
{
    protected $request, $relation;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->relation = new Relation;
    }

    public function index()
    {
        return view('insertfile');
    }

    public function findByID()
    {
        if($this->request->has('id'))
        {
            return $this->relation->find($this->request->input('id'));
        }
        abort(500, 'No ID provided');
    }

    public function findByName()
    {
        if($this->request->has('name'))
        {
            return $this->relation
                ->where('name', 'like',$this->request->input('name'))
                ->get();
        }
        abort(500, 'No name provided');
    }

    public function findByParent()
    {
        if($this->request->has('parent'))
        {
            return $this->relation
                ->where('parent', '=',$this->request->input('parent'))
                ->get();
        }
        abort(500, 'No parent ID provided');
    }

    public function createCategory()
    {
        if($this->request->has('name') &&
            $this->request->has('id') &&
            $this->request->has('parent'))
        {
            # Store in DB
            try {
                $this->relation->insert(['id' => $this->request->input('id'),
                    'parent' => $this->request->input('parent'),
                    'name' => $this->request->input('name'),
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now()]);
                return 'Record added';
            } catch (QueryException $e) {
                return $e->errorInfo;
            }
        }
        abort(500, 'Some or all of the required input fields are not filled');
    }

    public function updateCategory()
    {
        if($this->request->has('name') &&
            $this->request->has('id') &&
            $this->request->has('parent'))
        {
            try {
                $this->relation
                ->where('id', '=',$this->request->input('id'))
                ->update(['parent' => $this->request->input('parent'),
                    'name' => $this->request->input('name'),
                    'updated_at' => Carbon::now()]);
                return 'Category updated';
            } catch (QueryException $e) {
                return $e->errorInfo;
            }
        }
        abort(500, 'No name, ID or parent provided');
    }

    public function removeCategory()
    {
       if($this->request->has('id'))
       {
           try {
               $this->relation
                   ->where('ID', '=', $this->request->input('id'))
                   ->delete();
               return ('ID ' . $this->request->input('id') . ' is deleted, if it existed');
           } catch (QueryException $e) {
               return $e->errorInfo;
           }
       }
        abort(500, 'No ID in input');
    }

    public function insert()
    {
        if($this->request->hasFile('structured_tree')){
            # Get file
            $content = (new UploadFileController)->getFileSplit($this->request);

            # Make arr suitable for model
            $arr = $this->create_arr_suitable_for_model($content);

            # Store in DB
            try {
                $this->relation->insert($arr);
            } catch (QueryException $e) {
                return $e->errorInfo;
            }

            return 'File inserted to database';
        }
        else{
            abort(500, 'No File included');
        }
    }


    private function create_arr_suitable_for_model($arr)
    {
        $new_arr = [];
        foreach ($arr as $item){
            $a['ID'] = $item[0];
            $a['parent'] = $item[1];
            $a['name'] = preg_replace("/[^A-Za-z0-9 ]/", '', $item[2]);
            $a['created_at'] = Carbon::now();
            $a['updated_at'] = Carbon::now();

            $new_arr[] = $a;
        }
        return $new_arr;
    }
}
