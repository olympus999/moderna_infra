@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="app">
            <input type="hidden" value="/findbyid">
            <input type="number" min="0" placeholder="ID" v-model="data.id">
            <button v-on:click="sendDataToAPI" class="tiny radius">Find by ID</button>
        </div>
        <div class="app">
            <input type="hidden" value="/findbyname">
            <input type="text" placeholder="name" v-model="data.name">
            <button v-on:click="sendDataToAPI" class="tiny radius">Find by name</button>
        </div>
        <div class="app">
            <input type="hidden" value="/findbyparent">
            <input type="number" min="0" placeholder="parent" v-model="data.parent">
            <button v-on:click="sendDataToAPI" class="tiny radius">Find by parent</button>
        </div>
        <div class="app">
            <input type="hidden" value="/createcategory">
            <input type="number" min="0" placeholder="ID" v-model="data.id">
            <input type="number" min="0" placeholder="parent" v-model="data.parent">
            <input type="text" placeholder="name" v-model="data.name">
            <button v-on:click="sendDataToAPI" class="tiny radius">Create new category</button>
        </div>
        <div class="app">
            <input type="hidden" value="/updatecategory">
            <input type="number" min="0" placeholder="ID" v-model="data.id">
            <input type="number" min="0" placeholder="parent" v-model="data.parent">
            <input type="text" placeholder="name" v-model="data.name">
            <button v-on:click="sendDataToAPI" class="tiny radius">Update category</button>
        </div>
        <div class="app">
            <input type="hidden" value="/removecategory">
            <input type="number" min="0" placeholder="ID" v-model="data.id">
            <button v-on:click="sendDataToAPI" class="tiny radius">Remove category</button>
        </div>

        <div id="text-response">
            <br>
            <span>Returned data/info</span>
            <textarea class='form-control form-rounded col-6' id="response"></textarea>
        </div>
    </div>
    <script type="text/javascript">
        // The object is added to a Vue instance
        const nameHandlers = Array.from(document.querySelectorAll(".app"));
        for (let handler of nameHandlers) {
            new Vue({
                el: handler,
                data: {
                    data: {
                        endpoint: handler.getElementsByTagName('input')[0].value,
                        id: '',
                        parent: '',
                        name: ''
                    },
                },
                methods: {
                    sendDataToAPI: function (event) {
                        var data = this.data;
                        this.$http.post(data.endpoint, data).then(function (response) {
                            document.getElementById('response').value = JSON.stringify(response.body)
                            this.loading = false;
                        }, function (response) {
                            document.getElementById('response').value = 'ERROR: ' + JSON.stringify(response.body);
                            this.loading = false;
                        });
                    }
                }
            })
        }

        Vue.http.interceptors.push(function(request, next) {

            // modify headers
            request.headers.set('X-CSRF-TOKEN', '{!! csrf_token() !!}');

            // continue to next interceptor
            next();
        });
    </script>
@endsection