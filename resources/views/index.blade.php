<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/main.css"
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin">Admin</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/uploadfile">View relations</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Insert</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>
<div class="container">
    <div class="app">
        <input type="hidden" value="/findbyid">
        <input type="text" placeholder="ID" v-model="data.id">
        <button v-on:click="sendDataToAPI" class="tiny radius">Find by ID</button>
    </div>
    <div class="app">
        <input type="hidden" value="/findbyname">
        <input type="text" placeholder="name" v-model="data.name">
        <button v-on:click="sendDataToAPI" class="tiny radius">Find by name</button>
    </div>
    <div class="app">
        <input type="hidden" value="/findbyparent">
        <input type="text" placeholder="name" v-model="data.parent">
        <button v-on:click="sendDataToAPI" class="tiny radius">Find by parent</button>
    </div>
    <div class="app">
        <input type="hidden" value="/createcategory">
        <input type="text" placeholder="ID" v-model="data.id">
        <input type="text" placeholder="parent" v-model="data.parent">
        <input type="text" placeholder="name" v-model="data.name">
        <button v-on:click="sendDataToAPI" class="tiny radius">Create new category</button>
    </div>
    <div class="app">
        <input type="hidden" value="/updatecategory">
        <input type="text" placeholder="ID" v-model="data.id">
        <input type="text" placeholder="parent" v-model="data.parent">
        <input type="text" placeholder="name" v-model="data.name">
        <button v-on:click="sendDataToAPI" class="tiny radius">Update category</button>
    </div>
    <div class="app">
        <input type="hidden" value="/removecategory">
        <input type="text" placeholder="ID" v-model="data.id">
        <button v-on:click="sendDataToAPI" class="tiny radius">Remove category</button>
    </div>

    <div id="text-response">
        <br>
        <span>Returned data/info</span>
        <textarea class='form-control form-rounded col-6' id="response"></textarea>
    </div>
</div>
<script src="https://unpkg.com/vue"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.3.4"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

<footer>
<script>

    // The object is added to a Vue instance
    const nameHandlers = Array.from(document.querySelectorAll(".app"))
    for (let handler of nameHandlers) {
        new Vue({
            el: handler,
            data: {
                data: {
                    endpoint: handler.getElementsByTagName('input')[0].value,
                    id: '',
                    parent: '',
                    name: ''
                },
            },
            methods: {
                sendDataToAPI: function (event) {
                    var data = this.data;
                    this.$http.post(data.endpoint, data).then(function (response) {
                        console.log(response.body);
                        document.getElementById('response').value = JSON.stringify(response.body)
                        this.loading = false;
                    }, function (response) {
                        console.log('Error!:', response.data);
                        this.loading = false;
                    });
                }
            }
        })
    }

    Vue.http.interceptors.push(function(request, next) {

        // modify headers
        request.headers.set('X-CSRF-TOKEN', '{!! csrf_token() !!}');

        // continue to next interceptor
        next();
    });
</script>
</footer>
</body>