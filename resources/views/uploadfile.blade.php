@extends('layouts.master')

@section('content')
<?php
echo Form::open(array('url' => '/uploadfile','files'=>'true'));
echo 'Select the file to upload.';
echo Form::file('structured_tree');
echo '<br>';
echo '<br>';
echo Form::submit('Upload and print tree hierarchy');
echo Form::close();
?>
<div>
    <br>
    <span>
        Tree printed out:
    </span>
    <br>
    <br>
    @foreach ($tree as $leaf)
        <span>{{ $leaf }}</span>
        <br>
    @endforeach
</div>
@endsection
