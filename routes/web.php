<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function (){
    return redirect('home');
});

Route::get('/admin',function (){
    return view('admin');
});

Route::get('/home',function (){
    return view('home');
});

Route::get('/uploadfile',function (){
    return view('uploadfile', ['tree'=> []]);
});

/*
*  API endpoints
*/

# Task 1, upload file and return tree with correct padding
Route::post('/uploadfile','UploadFileController@showUploadFile');

# ID has to be in field 'id'
Route::post('/findbyid', 'EndpointController@findByID');

# Name needs to be in field 'name'
Route::post('/findbyname', 'EndpointController@findByName');

# Parent id needs to be in field 'parent'
Route::post('/findbyparent', 'EndpointController@findByParent');

# Create new category. Inputs need to have 'id', 'name' and 'parent'
Route::post('/createcategory', 'EndpointController@createCategory');

# For updating category one needs to input ID, name and parent.
# Where ID is used to find the record and then one can change name and parent
Route::post('/updatecategory', 'EndpointController@updateCategory');

# Remove category only needs ID as input
Route::post('/removecategory', 'EndpointController@removeCategory');